# @emanimation/strapi-utils
[semantic-release]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg

> A package containing various utils for use with Strapi applications and plugins.

## Install

``` shell
pnpm install @emanimation/strapi-utils
```

## Use

``` typescript
import { @emanimation/strapi-utils } from '@emanimation/strapi-utils'
// TODO: describe usage
```

## Author

Emmanuel Mahuni

## Related

TODO

## Acknowledgments

TODO

## License

ISC License

import { resolve } from 'path';
import { readFileSync } from 'fs';

// reliable way to get to a file, especially when using workspaces & pnp modules
// @ts-expect-error declarations
import where from 'where-are';

/**
 * get current strapi application/plugin package information
 * @param cwd current working directory
 * @param predicate predicate to use to determine whether to use the found package.json at given path
 * @type {{readonly uid: string, readonly name: any, readonly rootPath: any, readonly id: any, readonly pkg: any}}
 */
export function packageInfo (cwd: string | Function = process.cwd(), predicate?: Function) {
  let rootPath = where('package.json', cwd, predicate);
  if (!rootPath) throw new Error('Couldn\'t find package.json that doesn\'t belong to test-app package.');
  
  const pkg = JSON.parse(readFileSync(resolve(rootPath, 'package.json'), { encoding: 'utf8' }));
  return {
    rootPath,
    pkg,
    name: pkg.name,
    ...(pkg.strapi?.kind === 'plugin' ?
        {
          isPlugin: true,
          id:       pkg.name.replace(/^(@.*|strapi-)plugin-/i, ''),
          uid:      `plugin::${pkg.id}`,
        } : {}
    ),
  };
}

/**
 * infer the preferred package manager using lock files
 * @param predicate if provided, it is used to determine if where the package.json was found is acceptable
 * @returns {string | boolean}
 */
export function preferredPackageManager (predicate?: Function) {
  const lockPath = where(['./pnpm-lock.yaml', './yarn-lock.json', './yarn-lock.json'], predicate, true);
  if (!!lockPath) {
    return lockPath.includes('./pnpm-lock.yaml') ? 'pnpm' : lockPath.includes('./yarn-lock.json') ? 'yarn' : 'npm';
  }
  
  return false;
}
